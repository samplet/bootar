;;; Bootar
;;; Copyright 2020 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Bootar.
;;;
;;; Bootar is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Bootar is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public
;;; License along with Bootar.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; This script generates a description of the self-extracting Scheme
;;; (SES) archive that Guix knows how to build.  To build it, use
;;; 'guix build -f make-ses.scm'.
;;;
;;; The SES itself is just an a-list of filenames and contents,
;;; followed by a loop that writes the contents of each pair to disk
;;; in a file with the appropriate name.
;;;
;;; Code:

(use-modules (guix build utils)
             (guix gexp)
             (guix git-download)
             (guix packages)
             (ice-9 textual-ports))

(define struct-pack
  (origin
    (method git-fetch)
    (uri (git-reference
          (url "https://github.com/weinholt/struct-pack.git")
          (commit "v1.1.1")))
    (sha256
     (base32
      "0hd72m821pahjphzyjn26i55542v8makr55xzjll2cycja4wsbc1"))))

(define hashing
  (origin
    (method git-fetch)
    (uri (git-reference
          (url "https://github.com/weinholt/hashing.git")
          (commit "v1.3.0")))
    (sha256
     (base32
      "0rzgfbnk26kgdazh13gr28khdwrgsk1f9s2wfz7cr88bgbbyhsq6"))))

(define compression
  (origin
    (method git-fetch)
    (uri (git-reference
          (url "https://git.ngyro.com/r6rs-compression")
          (commit "a2d01f24d5ad703ed2742b97053d19dcd42f89b1")))
    (sha256
     (base32
      "0qn4lrzyslxm9m6fy7xdwjhbqp5v16z0x4b6p9vhkzjcd5s2icqx"))))

(computed-file
 "bootar.ses"
 #~(begin
     (use-modules (ice-9 match)
                  (ice-9 pretty-print)
                  (ice-9 textual-ports))
     (fluid-set! %default-port-encoding "UTF-8")
     (let ((files `(("bootar")
                    ("bootar/scripts")
                    ("bootar/scripts/bzip2.in"
                     . #$(local-file "scripts/bzip2.in"))
                    ("bootar/scripts/gzip.in"
                     . #$(local-file "scripts/gzip.in"))
                    ("bootar/scripts/tar.in"
                     . #$(local-file "scripts/tar.in"))
                    ("bootar/scripts/xz.in"
                     . #$(local-file "scripts/xz.in"))
                    ("bootar/hashing")
                    ("bootar/hashing/private")
                    ("bootar/hashing/private/common.scm"
                     . ,(string-append #$hashing "/private/common.sls"))
                    ("bootar/hashing/private/compat.scm"
                     . ,(string-append #$hashing "/private/compat.sls"))
                    ("bootar/hashing/fixnums.scm"
                     . ,(string-append #$hashing "/fixnums.sls"))
                    ("bootar/hashing/crc.scm"
                     . ,(string-append #$hashing "/crc.scm"))
                    ("bootar/hashing/hmac.scm"
                     . ,(string-append #$hashing "/hmac.scm"))
                    ("bootar/hashing/sha-2.scm"
                     . ,(string-append #$hashing "/sha-2.scm"))
                    ("bootar/compression")
                    ("bootar/compression/gzip.scm"
                     . ,(string-append #$compression "/gzip.scm"))
                    ("bootar/compression/sliding-buffer.scm"
                     . ,(string-append #$compression "/sliding-buffer.scm"))
                    ("bootar/compression/huffman.scm"
                     . ,(string-append #$compression "/huffman.scm"))
                    ("bootar/compression/tar.scm"
                     . ,(string-append #$compression "/tar.scm"))
                    ("bootar/compression/private")
                    ("bootar/compression/private/common.scm"
                     . ,(string-append #$compression "/private/common.scm"))
                    ("bootar/compression/private/compat.scm"
                     . ,(string-append #$compression "/private/compat.sls"))
                    ("bootar/compression/inflate.scm"
                     . ,(string-append #$compression "/inflate.scm"))
                    ("bootar/compression/bitstream.scm"
                     . ,(string-append #$compression "/bitstream.scm"))
                    ("bootar/compression/lzma.scm"
                     . ,(string-append #$compression "/lzma.scm"))
                    ("bootar/compression/lzma2.scm"
                     . ,(string-append #$compression "/lzma2.scm"))
                    ("bootar/compression/xz.scm"
                     . ,(string-append #$compression "/xz.scm"))
                    ("bootar/compression/bzip2.scm"
                     . ,(string-append #$compression "/bzip2.scm"))
                    ("bootar/struct")
                    ("bootar/struct/pack.scm"
                     . ,(string-append #$struct-pack "/pack.sls")))))
       (with-output-to-file #$output
         (lambda ()
           (print-disable 'escape-newlines)
           (write '(use-modules (ice-9 match)))
           (newline)
           (pretty-print
            `(let ((files ',(map (match-lambda
                                   ((directory) (list directory))
                                   ((name . filename)
                                    (call-with-input-file filename
                                      (lambda (port)
                                        (cons name
                                              (get-string-all port))))))
                                 files)))
               (for-each (match-lambda
                           ((directory) (mkdir directory))
                           ((filename . contents)
                            (call-with-output-file filename
                              (lambda (port)
                                (display contents port)))))
                         files)))
           (newline))))))
